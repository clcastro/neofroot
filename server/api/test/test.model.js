'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    User = require('../user/user.model'),
    User = require('./page.model');

var TestSchema = new Schema({
  userid : { type: Schema.ObjectId, ref: 'User' },
  created : Date,
  completedOn: Date, 
  urlRequest : String,
  urlProcessed : String,
  opts:{ pagegroup:String,period:String,notification:String},
  status: { type: String, default:'created' },
  crawl:{
  			limit:{ type: Number, default:10 },
  			startedOn: Date,
  			completedOn: Date,
  			pages: [{ type: Schema.ObjectId, ref: 'Page'}]
  		}
});

module.exports = mongoose.model('Test', TestSchema);