'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    User = require('../user/user.model'),
    Test = require('./test.model');;

var PageSchema = new Schema({
  testid : { type: Schema.ObjectId, ref: 'Test' },
  url: String,
  title: String,
  created : Date
});

module.exports = mongoose.model('Page', PageSchema);