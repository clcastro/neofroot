'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    User = require('../user/user.model'),
    Page = require('./page.model'),
    Test = require('./test.model');

var RunSchema = new Schema({
  testid : { type: Schema.ObjectId, ref: 'Test' },
  created : Date,
  lastModified: Date,
  results: [  {
  			  	pageid : { type: Schema.ObjectId, ref: 'Page'},
  			  	minion : String,
  			  	result: Object
  			  }
  		   ]
});

module.exports = mongoose.model('Run', RunSchema);