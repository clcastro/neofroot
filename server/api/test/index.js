'use strict';

var express = require('express');
var controller = require('./test.controller');
var auth = require('../../auth/auth.service');


var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.get('/:id/runs', controller.runsindex);
//router.get('/:id/run/:runid', controller.runsshow);
router.get('/:id/runs/last', controller.runslastshow);

module.exports = router;