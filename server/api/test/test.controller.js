//'use strict';

var _ = require('lodash');
var Test = require('./test.model');
var Page = require('./page.model');
var Run = require('./run.model');
var request = require('request');
var Crawler = require("simplecrawler");
var Url = require('url');
var cheerio = require('cheerio');
var async = require("async");


exports.socket = {emit:function(a,b){ console.log("I do nothing if socket is not properly configured",a,b); }};

// Get list of tests
exports.index = function(req, res) {
  Test.find(function (err, tests) {
    if(err) { return handleError(res, err); }
    return res.json(200, tests);
  });
};

// Get a single test
exports.show = function(req, res) {
 
  if(req.query.loadpages == 'true'){
    
    Test.findById(req.params.id).populate("crawl.pages").exec(function (err, test) {
      if(err) { return handleError(res, err); }
      if(!test) { return res.send(404); }
      return res.json(test);
    });

  }
  else{
     
    Test.findById(req.params.id, function (err, test) {
      if(err) { return handleError(res, err); }
      if(!test) { return res.send(404); }
      return res.json(test);
    });
  }
};

// Creates a new test in the DB.
exports.create = function(req, res) {

  var url = req.body.url;
  if(!url.match(/^(http:\/\/|https:\/\/)/))
    url = 'http://' + url;
  console.log("url:",url);

  request({url:url}, function (error, response, body) {
      if(error){
        return handleError(res, err);
      }
      
      if(response.statusCode == 200  ){
        req.body.urlRequest = url;
        req.body.urlProcessed = response.request.uri.href;
        req.body.created = new Date();
        
        Test.create(req.body, function(err, test) {
          console.log(test);
         
          res.cookie('mytests', req.cookies.mytests + ',' +test._id  ); 

          if(err) { return handleError(res, err); }
          if(test.opts.pagegroup == 'home')
          {
            test.status = 'crawling';
            test.save(function(err,test){ 
              getSocket().emit('test:status:'+test._id, test.status);
            });

            $ = cheerio.load(body);
            var title = $("head").find("title").text();           
            var Pages = [];
            var page  = { testid: test._id, url: url, title: title, created: new Date() };
            Pages.push(page);
            
            Page.collection.insert(Pages, function(collerr,docs){
                
                if(collerr)  return handleError(res, collerr);
                test.crawl.pages = _.pluck(docs,'_id');
                test.status = 'crawling-completed';
                test.save(function(err,t){
                  getSocket().emit('test:status:'+test._id, t.status);
                  console.log('saved test');
                  runTest(test._id);
                 
                });
            }); 
          }
          else
          {
            test.status = 'crawling';
            test.save(function(err,test){ 
              getSocket().emit('test:status:'+test._id, test.status);
            });
            crawl(test._id,function(error,result){
              console.log(err,result);
              if(err ){
                if(err.error && err.error == 'timeouterror')
                {
                  //do nothing
                }else{
                  test.status = 'crawling-error';
                  test.save(function(err,test){   getSocket().emit('test:status:'+test._id, test.status);  });   
                }

              }
              var Pages = [];
              _.each(result.pages,function(item,index){
                  console.log(index,item);
                  var page  = {
                                          testid: test.id,
                                          url: item.url,
                                          title: item.title,
                                          created: new Date()
                                        };
                  Pages.push(page);
              });
            
              process.nextTick(function(){
                Page.collection.insert(Pages, function(err,docs){
                    console.log("Saving collection",err,docs);
                    if(err)  return handleError(res, err);
                    test.crawl.pages = _.pluck(docs,'_id');
                    test.status = 'crawling-completed';
                    test.save(function(err,t){
                      getSocket().emit('test:status:'+test._id, t.status);
                      console.log('saved test');
                      runTest(test._id);
                     
                    });
                });
              });

            });
          }

          return res.json(201, test);  
        });
      }
      else{
           return res.json(400, {'status':'error','reason':'notfound'});
      }


    });

};

// Updates an existing test in the DB.
exports.update = function(req, res) {
  console.log(req.params.id,req.body);
  if(req.body._id) {  req.body._id; }
  Test.findById(req.params.id, function (err, test) {
    if (err) { return handleError(res, err); }
    if(!test) { return res.send(404); }
    console.log(test.userid,req.body.userid);
    if(test.userid)  { delete req.body.userid; }
    console.log(req.body);
    var updated = _.merge(test, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, test);
    });
  });
};

// Deletes a test from the DB.
exports.destroy = function(req, res) {
  Test.findById(req.params.id, function (err, test) {
    if(err) { return handleError(res, err); }
    if(!test) { return res.send(404); }
    test.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};


//router.get('/:id/run', controller.runsindex);
//router.get('/:id/run/:runid', controller.runsshow);
//router.get('/:id/run/last', controller.runslastshow);


exports.runsindex = function(req, res) {
  Run.find({testid: req.params.id}).exec(function (err, runs) {
    if(err) { return handleError(res, err); }
    if(!runs) { return res.send(404); }
    return res.json(runs);
  });
}
exports.runslastshow = function(req, res) {
  Run.findOne({testid: req.params.id}).sort('-created').exec(function (err, run) {
    if(err) { return handleError(res, err); }
    if(!run) { return res.send(404); }
    return res.json(run);
  });
}


var crawl = function(testid,callback){
   Test.findById(testid, function (err, test) {
    if(err) { return callback({err:err},null); }
    if(!test) { return callback({err:'404'},null); }
    
    try{
      var url = Url.parse(test.urlProcessed);
      console.log("url",url);
      var crawler = new Crawler(url.host,url.path);
    }catch(e){
      delete crawler;
      return callback(e,null);
    }

    crawler.scanSubdomains = true; 
    crawler.downloadUnsupported = false;
    crawler.addFetchCondition( function(parsedURL) {
        return !parsedURL.path.match(/\.(pdf|png|otf|jpg|gif|jpeg|svg|eot|woff|ttf|htc|js|css)$/i);
    });

    var crawlTimeout = setTimeout(function(){
          if(debug){ console.log("[timeout] "); } 
          crawler.stop();      
          callback({error:"timeouterror"},{pages: pages,errors: notfoundpages,totalFiles: crawler.queue.length } );//since complete event is not called when the crawler is stoped                   
          return false;
    },1000 * 20);    

    var debug = false;
    var pages = [];
    var notfoundpages = [];

    crawler                    
        .on("fetchcomplete",function(queueItem, responseBuffer , response ){          

        var url = queueItem.url;          
        
        if(queueItem.stateData  && queueItem.stateData.contentType &&  queueItem.stateData.contentType.match(/text\/html/gi) ){
          if(pages.length + 1 > test.crawl.limit){  
            return;
          }   
          
          $ = cheerio.load(responseBuffer.toString());
          var title = $("head").find("title").text();           
        var page = {};
        page.url = queueItem.url;
        page.title = title;
        page.httpStatus = queueItem.stateData.code;                    
        page.contentLength = queueItem.stateData.contentLength;
        page.contentType = queueItem.stateData.contentType;
     

        pages.push(page);
        if(pages.length + 1 > test.crawl.limit){
          if(debug){ console.log("[fetchcomplete][limit] ",url,pages.length); }
          crawler.stop();
          clearTimeout(crawlTimeout);
          callback(null,{ pages: pages,errors: notfoundpages,totalFiles: crawler.queue.length } );//since complete event is not called when the crawler is stoped
          return;
        }
        
      }
      else{
        console.log('not processing');  
      }
      console.log("CURRENT",pages.length);
        
      })
     .on("fetch404",function( queueItem, response ){          
        var notfoundpage = {};
        notfoundpage.url = queueItem.url; 
        notfoundpage.httpStatus = queueItem.stateData.code;            
        notfoundpages.push(notfoundpage);                  
        return true;
      })
      .on("fetcherror",function( queueItem, response ){           
         var notfoundpage = {};
        notfoundpage.url = queueItem.url; 
        notfoundpage.httpStatus = queueItem.stateData.code;            
        notfoundpages.push(notfoundpage);                  
        return true;
       })
      .on("fetchtimeout",function(  queueItem, crawlerTimeoutValue ){                 
        if(debug){ console.log("[fetchtimeout] ",queueItem.url,crawlerTimeoutValue); }
       })            
      .on("complete",function(){ 
                  
          if(debug){ console.log("[complete] "); }
          crawler.stop();
          clearTimeout(crawlTimeout);
          callback(null,{pages: pages,errors: notfoundpages,totalFiles: crawler.queue.length } );//since complete event is not called when the crawler is stoped        
        
          return false;
      })  

      .on("fetchclienterror",function(queueItem,errorData){ 
          if(debug){ console.log("[fetchclienterror][error] "); console.error(errorData.stack); console.log(queueItem); }
          crawler.stop();
          clearTimeout(crawlTimeout);
          callback({error:"fetchclienterror"},{pages: pages,errors: notfoundpages,totalFiles: crawler.queue.length } );//since complete event is not called when the crawler is stoped                    
          return false;
       })


      .on("queueerror",function(errorData , URLData){  if(debug){ console.log("[queueerror]",errorData); console.error(errorData.stack) }   })
      .on("queueadd",function(queueItem){ if(debug && false){ console.log("[queueadd] ",queueItem.url); }  })
      .on("fetchredirect",function(queueItem, parsedURL, response){  if(debug){ console.log("[fetchredirect]",queueItem.url,parsedURL);  } })
      .on("crawlstart",function(){  if(debug){ console.log("[crawlstart] "); }   })
      .on("fetchstart",function(){  if(debug){ console.log("[fetchstart] "); }   })
      .on("fetchheaders",function(){  if(debug){ console.log("[fetchheaders] "); }   })
      .on("discoverycomplete",function(queueItem, resources ){   if(debug){ console.log("[discoverycomplete] ",queueItem.url); }  })
      .on("fetchdataerror",function(queueItem, response){  if(debug){ console.log("[fetchdataerror] ",queueItem.url); }   });        
      
      crawler.start();      

  });
}

function runTest(testid)
{
  Test.findById(testid).populate('crawl.pages').exec(function(err, test) {
    if(test){
      test.status = 'running-test';
      test.save(function(error,t){   getSocket().emit('test:status:'+test._id, t.status);  });

      Run.create({ testid : test._id,created : new Date() },function(err,run){
/////////
        // 1st para in async.each() is the array of items
        async.each(test.crawl.pages,
          // 2nd param is the function that each item is passed to
          function(page, callback){
            // Call an asynchronous function, often a save() to DB
            checkPagespeed('desktop',run,page,function(){
              checkPagespeed('mobile',run,page,function(){
                callback();
              })
            })
          },
          // 3rd param is the function to call when everything's done
          function(err){
            // All tasks are done now  
            console.log("Processing is complete");
            test.status = 'run-completed';
            test.save(function(error,t){ 
              getSocket().emit('test:status:'+t._id, t.status);   });
          }
        );

/////////
      });

    }
  });
}
function checkPagespeed(strategy,run,page,callback)
{
            request(
                {
                    url: 'https://www.googleapis.com/pagespeedonline/v1/runPagespeed',
                    method: 'GET',
                    qs: {
                        'url':page.url,
                        'key': 'AIzaSyBNskxT8SAXwaBXslwLZFpstzTB8mNOZB0',
                        'strategy': strategy

                      }
                }, 
                function (error, response, body) { 
                  if (!error && response.statusCode == 200) {
                      var pagespeed = JSON.parse(body);
                      Run.findByIdAndUpdate(
                              run._id,
                              {$push: {results: { 
                                  pageid: page._id,
                                  minion:'pagespeed-'+strategy,
                                  result: pagespeed
                               }  },lastModified:new Date() },
                              {safe: true, upsert: true},
                              function(err, runs) {
                                /////????????

                                  callback();

                                /////????????
                              }
                          );     
                  }
                  else{
                    // Async call is done, alert via callback
                    callback();
                  }
                }
            );
}

function handleError(res, err) {
  return res.send(500, err);
}
function getSocket(){
  return exports.socket;
}