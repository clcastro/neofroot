'use strict';

angular.module('frootboxApp')
  .controller('ConfigureCtrl', function ($scope,$http,$stateParams,socket,$location,Auth) {
    $scope.test = {
    				_id:'',
    				userid: '',
            opts:{
              pagegroup:"website",
              period:"weekly",
              notification:"email",
              dateopts:{
                      day:0,
                      time:{hours:0,GMToffset:0}
              },
              pages:[]
            }
          };
    $scope.lastPageRemoved = null;
    $scope.onPageLimit = false;

    $scope.hours = [
    				{label:"~00:00 AM",value:0},
    				{label:"~01:00 AM",value:1},
    				{label:"~02:00 AM",value:2},
    				{label:"~03:00 AM",value:3},
    				{label:"~04:00 AM",value:4},
    				{label:"~05:00 AM",value:5},
    				{label:"~06:00 AM",value:6},
    				{label:"~07:00 AM",value:7},
    				{label:"~08:00 AM",value:8},
    				{label:"~09:00 AM",value:9},
    				{label:"~10:00 AM",value:10},
    				{label:"~11:00 AM",value:11},
    				{label:"~12:00 PM",value:12},
    				{label:"~01:00 PM",value:13},
    				{label:"~02:00 PM",value:14},
    				{label:"~03:00 PM",value:15},
    				{label:"~04:00 PM",value:16},
    				{label:"~05:00 PM",value:17},
    				{label:"~06:00 PM",value:18},
    				{label:"~07:00 PM",value:19},
    				{label:"~08:00 PM",value:20},
    				{label:"~09:00 PM",value:21},
    				{label:"~10:00 PM",value:22},
    				{label:"~11:00 PM",value:23}

    			   ];
  $scope.gmts = [
            {label:"GMT-12",value:-12},
            {label:"GMT-11",value:-11},
            {label:"GMT-10",value:-10},
            {label:"GMT-9",value:-9},
            {label:"GMT-8",value:-8},
            {label:"GMT-7",value:-7},
            {label:"GMT-6",value:-6},
            {label:"GMT-5",value:-5},
            {label:"GMT-4",value:-4},
            {label:"GMT-3",value:-3},
            {label:"GMT-2",value:-2},
            {label:"GMT-1",value:-1},
            {label:"GMT",value:0},
            {label:"GMT+1",value:1},
            {label:"GMT+2",value:2},
            {label:"GMT+3",value:3},
            {label:"GMT+4",value:4},
            {label:"GMT+5",value:5},
            {label:"GMT+6",value:6},
            {label:"GMT+7",value:7},
            {label:"GMT+8",value:8},
            {label:"GMT+9",value:9},
            {label:"GMT+10",value:10},
            {label:"GMT+11",value:11},
            {label:"GMT+12",value:12},
            {label:"GMT+13",value:13},
            {label:"GMT+14",value:14}

             ];
   	$scope.speedAllScores = [];
   	for(var i = 100; i> 0; i-= 5){
   		$scope.speedAllScores.push(i);
   	}

    $http.get('/api/tests/'+$stateParams.testid+"?loadpages=true").success(function(test) {
        
        //We need to assign the test.options
        console.log(test);

        $scope.test._id = test._id;
      	$scope.test.userid = test.userid;
        $scope.test.opts.pagegroup = test.opts.pagegroup;
        $scope.test.opts.period = test.opts.period;
        $scope.test.opts.notification = test.opts.notification;


        var created = new Date(test.created);
      
        $scope.test.opts.dateopts.day = created.getDay();
        $scope.test.opts.dateopts.time.hours = created.getHours();
        $scope.test.opts.dateopts.time.GMToffset = Math.floor(created.getTimezoneOffset()/60);


      	//$scope.test.pages = test.crawl.pages;
      	if($scope.test.opts.pages.length  == 0)
      	{
      		angular.forEach(test.crawl.pages,function(item,index){
      			$scope.test.opts.pages.push({order:index + 1, id: item._id,title:item.title, url:item.url,originalurl:item.url,validated:true,blocked:0 == index,removed:false });
      		});
      	}
     });

	$scope.changeOption = function(opt,value,soon)
    {
      if(soon) return false;
      switch(opt){
        case "optype":
          $scope.test.opts.pagegroup = value;
          if(value == 'website' && $scope.test.opts.period == 'daily')
            $scope.test.opts.period = 'weekly';
        break;
        case "opperiod":
          $scope.test.opts.period = value;
          if(value == 'daily' && $scope.test.opts.pagegroup == 'website')
            $scope.test.opts.pagegroup = 'home';

        break;
        case "opnotification":
          $scope.test.opts.notification = value;
        break;
      }
    }
    $scope.removeOptionPage = function(){
    	this.page.removed = true;
    	$scope.lastPageRemoved = this.page;
    	$scope.reorderOptionPages();
    }
    $scope.addOptionsPageAfterThis = function(){
		var lenew = {order:0, id: '',title:'-', url:'',validated:false,blocked:false,removed:false };
		$scope.test.opts.pages.splice($scope.test.opts.pages.indexOf(this.page) + 1, 0, lenew);
		$scope.reorderOptionPages();
    }
    $scope.reorderOptionPages = function(){
    	var i  = 0;
    	angular.forEach($scope.test.opts.pages,function(item,index){
      		item.order = i +1;
      		if(!item.removed) ++i; 		
      	});
      	console.log(i);
      	if(i >= 18) $scope.onPageLimit = true;
      	else $scope.onPageLimit = false;
    }
    $scope.getHours = function(){
    	var hours = [];
    	for(var i = 0; i<=12 ; i++)
    	{
    		hours.push({label: i + " AM",hour:i});
    	}
    	for(var i = 1; i<12 ; i++)
    	{
    		hours.push({label: i + " PM",hour:i+12});
    	}
    	console.log(hours);
    	return hours;
    }
    $scope.changeOptionDay = function(day)
    {
      $scope.test.opts.dateopts.day = day;
      console.log($scope.test.opts.dateopts);
    }

  });
