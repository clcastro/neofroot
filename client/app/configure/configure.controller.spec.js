'use strict';

describe('Controller: ConfigureCtrl', function () {

  // load the controller's module
  beforeEach(module('frootboxApp'));

  var ConfigureCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConfigureCtrl = $controller('ConfigureCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
