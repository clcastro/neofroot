'use strict';

angular.module('frootboxApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('configure', {
        url: '/check/configure/:testid',
        templateUrl: 'app/configure/configure.html',
        controller: 'ConfigureCtrl'
      });
  });