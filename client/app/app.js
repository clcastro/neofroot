'use strict';

angular.module('frootboxApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  })

  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  })

  .run(function ($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    NotifyDemo($rootScope);

    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });



function NotifyDemo ($scope) {
    var win = window,
        statusClass = {},
        isIE = false,
        isSupported = notify.isSupported,
        messages = {
            notPinned: 'Pin current page in the taskbar in order to receive notifications',
            notSupported: '<strong>Desktop Notifications not supported!</strong>'
        };
    
    $scope.notification = {
        title: "Notification Title",
        body: "Notification Body",
        icon: "/favicon.png"
    };
    $scope.permissionLevel = notify.permissionLevel();
    $scope.permissionsGranted = ($scope.permissionLevel === notify.PERMISSION_GRANTED);

    try {
        isIE = (win.external && win.external.msIsSiteMode() !== undefined);
    } catch (e) {}

   
    $scope.requestPermission = function() {
        if ($scope.permissionLevel === notify.PERMISSION_DEFAULT) {
            notify.requestPermission(function() {
                $scope.$apply($scope.permissionLevel = notify.permissionLevel());
                $scope.$apply($scope.permissionsGranted = ($scope.permissionLevel === notify.PERMISSION_GRANTED));
                $scope.$apply($scope.status = isSupported ? statusClass[$scope.permissionLevel] : statusClass[notify.PERMISSION_DENIED]);
                $scope.$apply($scope.message = isSupported ? (isIE ? messages.notPinned : messages[$scope.permissionLevel]) : messages.notSupported);
            });
        }
    }
    
    $scope.showNotification = function(title,body) {
        notify.createNotification(title, {
            body: body,
            icon: $scope.notification.icon
        });
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}


function checkIfThisIsMyTest(test_id)
{
  var ids = decodeURIComponent(getCookie('mytests')).split(",");
  var found = false;
  angular.forEach(ids, function(res,index){
    if(res.trim() == res)
      found = true;
  });
  return found ;
}