'use strict';

angular.module('frootboxApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('results', {
        url: '/check/results/:testid',
        templateUrl: 'app/results/results.html',
        controller: 'ResultsCtrl'
      });
  });