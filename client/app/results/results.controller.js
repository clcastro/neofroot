'use strict';

angular.module('frootboxApp')
  .controller('ResultsCtrl', function ($scope,$http,$stateParams,socket,$location,Auth) {
    //Initialize the socket to hear just the test 
    $scope.test = {};
    $scope.currentPage = {};
    $scope.currentPageResults = {};
    $scope.currentStatus = "loading";
    $scope.currentRun = {};
    $scope.pagesResult = [];
    $scope.pid = {};

    $http.get('/api/tests/'+$stateParams.testid+"?loadpages=true").success(function(test) {
        $scope.test = test;
        if(test.status == "run-completed"){
            $scope.loadLastRunAndShowit();
        }
        else{
            $scope.updateTestStatus(test.status);
        }
     	socket.socket.on("test:status:"+test._id,function(status){
     		
            if(status == "run-completed"){
                 $scope.loadLastRunAndShowit();
                 $scope.showNotification("Frootbox","Test completed!");
            }
            else{
                $scope.updateTestStatus(status);
            }
     	});
     });

    $scope.updateTestStatus = function(status)
    {
        $scope.currentStatus = status;
    }

    $scope.loadLastRunAndShowit = function()
    {
        console.log('loadLastRunAndShowit');
        if($scope.test.crawl.pages.length == 0)
        {
            
            $http.get('/api/tests/'+$stateParams.testid+"?loadpages=true" ).success(function(test) {
               console.log('loading pages on loadLastRunAndShowit');
                $scope.test = test;
                $scope.currentStatus = test.status ;
                cont();
            });
        } else {
            
            cont();
        }

        function cont(){

             console.log('cont',$scope.currentStatus);
            $scope.currentPage = $scope.test.crawl.pages[0];

            $http.get('/api/tests/'+$stateParams.testid+'/runs/last').success(function(run) {
                console.log('loading pages on cont');
                $scope.currentRun = run;
                console.log("run",$scope.currentRun)
                $scope.createListResults();
                $scope.showCurrentResults();
                //We are forcing the state because if we are loading the run,
                //is because the test is completed but we might not have the test status as completed
                $scope.currentStatus = 'run-completed';
                console.log("status",$scope.test.status)

            });
        }
        
    }

    $scope.createListResults = function(){
       
        console.log('createListResults');

        angular.forEach($scope.test.crawl.pages, function(res,index){
            var page = {_id:res._id,url:res.url,title:res.title ,results:{}};
            
            angular.forEach($scope.currentRun.results, function(runres,runindex){
               
                if( page._id == runres.pageid )
                {
                    page.results[runres.minion.replace('pagespeed-','')] = runres.result.score;
                }
            });

            $scope.pagesResult.push(page);
        });

    }


    $scope.changeCurrent = function(){
         console.log('changeCurrent');

        $scope.pid = this.page._id;  
        angular.forEach($scope.test.crawl.pages, function(res,index){
          
            if(res._id == $scope.pid)
            {   
                
                $scope.currentPage = res;
            }
        });

        $scope.showCurrentResults(); 
    }
    $scope.changePage = function(){
         $scope.showCurrentResults();    
    }
    $scope.showCurrentResults = function(){
         console.log('showCurrentResults');
        $scope.currentPageResults = {};
        angular.forEach($scope.currentRun.results, function(res,index){
            if(res.pageid == $scope.currentPage._id)
            {
                $scope.currentPageResults[res.minion.replace('pagespeed-','')] = res.result;
            }
        });
       
    }
    $scope.isUserLoggedIn = function(){
        return  Auth.isLoggedIn();
    }
    $scope.isMyTestOrCanIOwnThisTest = function(){
        
        if(Auth.isLoggedIn() &&  Auth.getCurrentUser()._id  == $scope.test.userid){
            return true;
        }else{
            return $scope.checkIfThisIsMyTest($scope.test._id);
        }
        return false;
    }
    $scope.isTestConfigured = function(){
        return false;
    }


    $scope.openRule = function($childScope){
        $childScope.result.expanded = !$childScope.result.expanded ? true:false; 
    }
    $scope.openBlock = function($childScope){
        $childScope.block.expanded = !$childScope.block.expanded ? true:false; 
    }
    

    $scope.gotoStep4 = function(){
        if(!Auth.isLoggedIn() && checkIfThisIsMyTest($scope.test._id)){
            $location.path("/check/own/"+$scope.test._id);
        }
        else if(Auth.isLoggedIn() &&  Auth.getCurrentUser()._id  == $scope.test.userid){
            $location.path("/check/configure/"+$scope.test._id);
        }
        else{
            alert('Your are not athourized to do this');
        }
    }

    $scope.checkIfThisIsMyTest = function(test_id)
    {
      var ids = decodeURIComponent(getCookie('mytests')).split(",");
      var found = false;
      angular.forEach(ids, function(res,index){
        if(res == "") return;
        
        if(res.trim() == res){
          found = true;
        }
      });
      return found ;
    }


  });
