'use strict';

angular.module('frootboxApp')
  .filter('formatps', function () {
    return function (input) {

      var args = arguments[1];
	  return input.replace(/\$(\d+)/g, function (match, capture) {
	  		var index =1*capture -1 ;
			return args[index].value;
	  });
    };
  })
   .filter('kindofround', function () {
    return function (input) {
	  return Math.ceil(input);
    };
  });
