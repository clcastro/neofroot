'use strict';

angular.module('frootboxApp')
  .controller('MainCtrl', function ($scope, $http, socket,$location,Auth) {
    
    $scope.test = {
                    opts:{
                      pagegroup:"website",
                      period:"weekly",
                      notification:"email"
                    },
                    url:'',
                    loading:false

                }
    $scope.errorcall = "";
    $scope.showOptions = function(opt){
      $(".for-"+opt).css({position:'fixed',display:'block',width:$("."+opt).outerWidth(),height:$("."+opt).height(),top: $("."+opt).offset().top + 6 ,left: $("."+opt).offset().left });
      $(".for-"+opt).addClass("active").animate({top:0,left:0,width:$(window).width(),height:$(window).height()},function(){
        $(".for-"+opt).addClass("current");
      });
      $(window).on("keyup",function(ev){
          if( ev.keyCode == 27 ){
                $(".for-"+opt).removeClass("current").css({position:'absolute'}).animate({width:$("."+opt).outerWidth(),height:$("."+opt).height(),top: $("."+opt).offset().top + 6 ,left: $("."+opt).offset().left },function(){
                  $(".for-"+opt).removeClass("active").css("display",'none');
                });
                $(window).off("keyup");
          }
      })
    } 
    $scope.changeOption = function(opt,value,soon)
    {
      $(window).off("keyup");
      if(soon) return false;
      switch(opt){
        case "optype":
          $scope.test.opts.pagegroup = value;
          if(value == 'website' && $scope.test.opts.period == 'daily')
            $scope.test.opts.period = 'weekly';
        break;
        case "opperiod":
          $scope.test.opts.period = value;
          if(value == 'daily' && $scope.test.opts.pagegroup == 'website')
            $scope.test.opts.pagegroup = 'home';

        break;
        case "opnotification":
          $scope.test.opts.notification = value;
        break;
      }
      
      

      $(".for-"+opt).removeClass("current").css({position:'absolute'}).animate({width:$("."+opt).outerWidth(),height:$("."+opt).height(),top: $("."+opt).offset().top + 6 ,left: $("."+opt).offset().left },function(){
        $(".for-"+opt).removeClass("active").css("display",'none');
      });

    }

    $scope.doStep1 = function(){
      if($scope.test.url.trim() == '') return  $scope.errorcall = "The URL cannot be empty";
      if(!ValidUrl($scope.test.url))  return  $scope.errorcall = "The URL is not valid";
      if($scope.test.loading) return false;
      $scope.requestPermission();
      $scope.errorcall = "";
      $scope.test.loading = true;
      
      $http.post('/api/tests',$scope.test).success(function(response) {

        Auth.isLoggedInAsync(function(loggedIn) {

          if (!loggedIn) {
            //$location.path("/check/own/"+response._id)
            $location.path("/check/results/"+response._id);;//for now we fastward to results
            $scope.test.loading = false;
          }
          else{
             Auth.isLoggedInAsync(function(logged){
              var user = Auth.getCurrentUser();
              console.log(user);
              var data = {userid:user._id};
              console.log(data);
              $http.put('/api/tests/'+response._id,data).success(function(res) {
                $scope.test.loading = false;
                $location.path("/check/results/"+res._id);
              });
             });
          }
        }); 
      }).error(function(err,code,serv,request){

        $scope.test.loading = false;
        if(err && err.code == 'ENOTFOUND'){
          $scope.errorcall = "Invalid url, or url not found";
        }else{
           $scope.errorcall = "Unknown Error";
        }
      });
    }

    $scope.$on('$destroy', function () {
     // socket.unsyncUpdates('thing');
    });



  });

function ValidUrl(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if(!pattern.test(str)) {
    return false;
  } else {
    return true;
  }
}
