'use strict';

angular.module('frootboxApp')
  .controller('OwnCtrl', function ($scope,$http, Auth,$location,$stateParams) {
  
   
    $scope.showLogin = function(){
    	$("#login-form").removeClass("hide");
    	$("#signup-form").addClass("hide");
    }
     $scope.showSignUp = function(){
    	$("#signup-form").removeClass("hide");
    	$("#login-form").addClass("hide");
    	
    }

    $scope.userlogin = {};
    $scope.usersignup = {};
    $scope.loginerrors = {};
    $scope.signuperrors = {};

    $scope.login = function(form1) {
      $scope.submitted = true;

      if(form1.$valid) {
        Auth.login({
          email: $scope.userlogin.email,
          password: $scope.userlogin.password
        })
        .then( function(token) {
          Auth.isLoggedInAsync(function(logged){
              var user = Auth.getCurrentUser();
              
              var data = {userid:user._id};
             
              $http.put('/api/tests/'+$stateParams.testid,data).success(function(response) {
                
                $location.path("/check/configure/"+response._id);
              });
          });
        
        })
        .catch( function(err) {
          $scope.loginerrors.other = err.message;
        });
      }
    };


    $scope.register = function(form2) {
      $scope.submitted = true;

      if(form2.$valid) {
        Auth.createUser({
          name: $scope.usersignup.name,
          email: $scope.usersignup.email,
          password: $scope.usersignup.password
        })
        .then( function() {
           Auth.isLoggedInAsync(function(logged){
              var user = Auth.getCurrentUser();
             
              var data = {userid:user._id};
           
              $http.put('/api/tests/'+$stateParams.testid,data).success(function(response) {
                
                $location.path("/check/configure/"+response._id);
              });
          });
        })
        .catch( function(err) {
          err = err.data;
          $scope.signuperrors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form2[field].$setValidity('mongoose', false);
            $scope.signuperrors[field] = error.message;
          });
        });
      }
    };

    

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };




  });
