'use strict';

describe('Controller: OwnCtrl', function () {

  // load the controller's module
  beforeEach(module('frootboxApp'));

  var OwnCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OwnCtrl = $controller('OwnCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
