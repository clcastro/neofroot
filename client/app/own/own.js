'use strict';

angular.module('frootboxApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('own', {
        url: '/check/own/:testid',
        templateUrl: 'app/own/own.html',
        controller: 'OwnCtrl'
      });
  });