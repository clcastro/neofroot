'use strict';

describe('Filter: nameoption', function () {

  // load the filter's module
  beforeEach(module('frootboxApp'));

  // initialize a new instance of the filter before each test
  var nameoption;
  beforeEach(inject(function ($filter) {
    nameoption = $filter('nameoption');
  }));

  it('should return the input prefixed with "nameoption filter:"', function () {
    var text = 'angularjs';
    expect(nameoption(text)).toBe('nameoption filter: ' + text);
  });

});
