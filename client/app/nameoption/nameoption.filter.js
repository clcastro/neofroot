'use strict';

angular.module('frootboxApp')
  .filter('nameoption', function () {
    return function (val,optiontype) {
		switch(optiontype){
			case 'type':
				switch(val){
					case 'website': return 'Website'; break;
					case 'home':  return 'Homepage'; break;
					case 'full': return 'Full Website'; break;
				}
			break;
			case 'period':
				switch(val){
					case 'weekly': return 'Weekly'; break;
					case 'daily':  return 'Daily'; break;
					case 'demmand': return 'On Demmand'; break;
				}
			break;
			case 'notification':
				switch(val){
					case 'email': return 'Email'; break;
					case 'sms':  return 'SMS'; break;
					case 'mobile': return 'Mobile Notifications'; break;
				}
			break;
		}
        return 'nameoption filter: ' + input;
    };
  });
