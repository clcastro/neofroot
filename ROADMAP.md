# Frootbox Roadmap#

#### General ####

* Improve logo
* Confirm font choices

##### Pages #####

* About Page

### Home ###

* Better graphics for options selection, also disable options that are not available yet - OK FOR NOW
* Improve error management
* Vote system for new feautures
* Improve overall look of the form

#### Responsive ####

* Make work options selection on mobile